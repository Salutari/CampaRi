---
title: "MST and SST. The way to SAPPHIRE"
author: "Davide Garolini"
date: "10 Jan 2018"
vignette: >
  %\VignetteEngine{knitr::knitr}
  %\VignetteIndexEntry{MST_SST_vignette}
  %\usepackage[utf8]{inputenc}
---



This tutorial is focused on the SAPPHIRE plot and its possible clustering and visualization properties within the package CampaRi.

## MST
We will use here the trajectory generated using the following command (needs original campari package):

```r
cat("STARTING VIGNETTE MST_SST_vignette...\n")
```

```
## STARTING VIGNETTE MST_SST_vignette...
```

```r
library(CampaRi)
# standard run of the simulation in tutarial 11 - In this case we copy pasted the nbu_simulation.key
data.table::fwrite(list("NBU", "END"), file = "nbu.in", sep = "\n")
run_campari(FMCSC_SEQFILE="nbu.in",# you must have it defined according to CAMPARI's rules
            # FMCSC_BASENAME="NBU", # lets try the base_name option
            base_name = "NBU", print_status = F,  silent = F, print_log_error = T,
            PARAMETERS="oplsaal.prm", # if this variable it is not supplied will be automatically assigned to <full path to folder>/campari/params/abs3.2_opls.prm
            FMCSC_SC_IPP=0.0,
            FMCSC_SC_BONDED_T=1.0,
            FMCSC_DYNAMICS=3,
            FMCSC_FRICTION=3.0,
            FMCSC_TIMESTEP=0.005,
            FMCSC_TEMP=400.0,
            FMCSC_NRSTEPS=1000,
            FMCSC_EQUIL=0,
            FMCSC_XYZOUT=1,
            FMCSC_XYZPDB=3,
            FMCSC_TOROUT=1,
            FMCSC_COVCALC=20000000,
            FMCSC_SAVCALC=20000000,
            FMCSC_POLCALC=20000000,
            FMCSC_RHCALC=20000000,
            FMCSC_INTCALC=20000000,
            FMCSC_POLOUT=20000000,
            FMCSC_ENSOUT=20000000,
            FMCSC_ENOUT=20000000,
            FMCSC_RSTOUT=20000000
)
```

```
## Looking for additional campari bin locations (exports) in ~/.bashrc file... found.
## Found the following possible exe location: /software/campariv3/bin/x86_64/ 
## Found the following possible exe location: /home/linuxbrew/.linuxbrew/lib/R/3.5/site-library/CampaRi/extdata/for_campari//debcampari-master/bin/x86_64 
## Final PATH env variable: /home/linuxbrew/.linuxbrew/lib/R/3.5/site-library/CampaRi/extdata/for_campari//debcampari-master/bin/x86_64:/home/linuxbrew/.linuxbrew/sbin:/home/linuxbrew/.linuxbrew/bin:/home/dgarolini/anaconda2/bin:/home/dgarolini/app/neuron/iv/x86_64/bin:/home/dgarolini/app/neuron/nrn/x86_64/bin:/opt/intel/compilers_and_libraries_2017.1.132/linux/bin/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/mpi/intel64/bin:/opt/intel/debugger_2017/gdb/intel64_mic/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/software/campariv3/bin/x86_64/:/home/dgarolini/app/nest/nest-2.10.0/ 
## Selected base name for every output/input files (WARNING: they will be overwritten):  NBU 
## SIMULATION RUN: time series (trj) or trajectory file (data_file) not inserted (not even some FMCSC_*FILE).
## /home/linuxbrew/.linuxbrew/lib/R/3.5/site-library/CampaRi/extdata/for_campari//debcampari-master/bin/x86_64/campari will be used for this CAMPARI run. 
## Inserted only filename in the PARAMETERS variable. This file will be searched in the exe directory. To use current directory please add "./" in front of the filename.
## Using the following specific PARAMETERS: /home/linuxbrew/.linuxbrew/lib/R/3.5/site-library/CampaRi/extdata/for_campari//debcampari-master/params/oplsaal.prm 
## Using the following specific sequence file: nbu.in 
## writing of keyfile NBU.key completed. 
## 
## Starting campari... 
##  --------------------------------------------------------------------------
##                                     CAMPARI                                
##  --------------------------------------------------------------------------
## If not in bakground mode, an error in CAMPARI will be reflected in R.
## Direct console printing disabled. Please check NBU.log  file for real time logging (at the end it will be tailed).
```

```r
# rename
file.rename("NBU_traj.dcd", to = "NBU.dcd")
```

```
## [1] TRUE
```

```r
file.rename("FYC.dat", to = "NBU.fyc")
```

```
## [1] TRUE
```

When loading a trajectory into R, run_campari() will use the ASCII support (producing a .tsv file) which is not able to select dihedral angles as the original run. Therefore we load directly NBU.fyc and use it (distance 1).


```r
library(data.table)
# to use ncminer we need to load fyc directly (dihedral angles handling not implemented)
trj <- data.table::fread("NBU.fyc", header = F, skip = 1, data.table = FALSE)[,-1]
# head(trj)
# fread("head -n 1 NBU.fyc") # head of it
trj <- sapply(trj, as.numeric) # always be sure that it is numeric!
hist(trj[,2]) # this should have 3 peaks per diheadral angle
```

![plot of chunk mst_run_campari](figure/mst_run_campari-1.png)

```r
str(trj)
```

```
##  num [1:1000, 1:3] 180 179 179 178 178 ...
##  - attr(*, "dimnames")=List of 2
##   ..$ : NULL
##   ..$ : chr [1:3] "V2" "V3" "V4"
```

```r
run_campari(trj = trj, base_name = "ascii_based_analysis",
            FMCSC_CPROGINDMODE=1, #mst
            FMCSC_CCOLLECT=1, print_status = F, silent = T, print_log_error = T, 
            FMCSC_CMODE=4,
            FMCSC_CDISTANCE=1,
            FMCSC_CPROGINDSTART=21, #starting snapshot 
            FMCSC_CRADIUS=10000,
            FMCSC_CCUTOFF=10000,
            FMCSC_CPROGINDWIDTH=1000) #local cut is automatically adjusted to 1/10 if it is too big (as here)
            #FMCSC_CPROGMSTFOLD 4 # b)
sapphire_plot(sap_file = "PROGIDX_000000000021.dat", timeline= T,title = "ORIGINAL CAMPARI - MST")
```

```
## The maximum height of the y axis is: 6.214608
```

![plot of chunk mst_run_campari](figure/mst_run_campari-2.png)

There is the possibility to use some internal replica of the main algorithms used for the metastable states recognition (basins in SAPPHIRE plots). The Minimum Spanning Tree, however, has memory limitation which are difficult to handle in R.


```r
# netcdf dumping is broken with the new versions of netcdf (> 4.1)
# with 100k it is possible that also the netcdf option could exceede the memory availability
mst_from_trj(trj = trj, dump_to_netcdf = T, mode = "fortran", distance_method = 1, silent = T)
ret <- gen_progindex(nsnaps = nrow(trj), read_from_netcdf = T, snap_start = 21, silent = T); cat(nrow(trj), "\n")
ret2 <- gen_annotation(ret, snap_start = 21, local_cut_width = 200, silent = T)
sapphire_plot(sap_file = 'REPIX_000000000021.dat', timeline = T,title = "CAMPARI WRAPPER - MST")
```


It is however possible to use directly the R data system without the netcdf dumping (of the minimum spannin tree). NOTE: the installation can change this behaviour.
Let's see an example:

```r
adjl <- mst_from_trj(trj=trj, silent = T)
# adjl2 <- contract_mst(adjl = adjl, n_fold = 5)
ret <- gen_progindex(adjl = adjl, snap_start = 21, silent = T)
# ret <- gen_progindex(nsnaps = 2000, read_from_netcdf = T, snap_start = 21)
ret2 <- gen_annotation(ret_data = ret, local_cut_width = 200, snap_start = 21, silent = T)
sapphire_plot(sap_file = 'REPIX_000000000021.dat', timeline = T, title = "CAMPARI WRAPPER - MST")
```

```
## The maximum height of the y axis is: 6.214608
```

![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-2-1.png)


It is also possible to use the SST which is a faster version to construct the spanning tree for the SAPPHIRE plot. The code in R was not updated to the latest version and was not reliable. For these reasons, we adopted a novel version which is only an interface/wrapper of the original code.


```r
SST_campari(trj, base_name = "basename", sap_file = "sap_file.dat", silent = T, multi_threading = F, rm_extra_files = T, 
            cmax_rad = NULL, cradius = NULL, pcs = NULL, state.width = 300, cdistance = 1, pi.start = 21, search.attempts = NULL, birchheight = NULL,
            leaves.tofold = NULL)
sapphire_plot(sap_file = "sap_file.dat", timeline= T,title = "ORIGINAL CAMPARI - MST")
```

```
## The maximum height of the y axis is: 6.214608
```

![plot of chunk unnamed-chunk-3](figure/unnamed-chunk-3-1.png)



let's do everything again with .xtc (starting from simulation even):


```r
# standard run of the simulation in tutarial 11 - In this case we copy pasted the nbu_simulation.key
run_campari(seq_in = "nbu.in",# you must have it defined according to CAMPARI's rules
            # FMCSC_BASENAME="NBU", # lets try the base_name option
            base_name = "NBU", print_status = F, print_log_error = T, # it will take 55 s in background ~
            PARAMETERS="oplsaal.prm", # if this variable it is not supplied will be automatically assigned to <full path to folder>/campari/params/abs3.2_opls.prm
            FMCSC_SC_IPP=0.0,
            FMCSC_SC_BONDED_T=1.0,
            FMCSC_DYNAMICS=3,
            FMCSC_FRICTION=3.0,
            FMCSC_TIMESTEP=0.005,
            FMCSC_TEMP=400.0,
            FMCSC_NRSTEPS=1000000,
            FMCSC_EQUIL=0,
            FMCSC_XYZOUT=100,
            FMCSC_XYZPDB=4, # xtc
            FMCSC_TOROUT=100,
            FMCSC_COVCALC=20000000,
            FMCSC_SAVCALC=20000000,
            FMCSC_POLCALC=20000000,
            FMCSC_RHCALC=20000000,
            FMCSC_INTCALC=20000000,
            FMCSC_POLOUT=20000000,
            FMCSC_ENSOUT=20000000,
            FMCSC_ENOUT=20000000,
            FMCSC_RSTOUT=20000000
)
# rename
# file.rename()
# system('mv NBU_traj.xtc NBU.xtc')
# system('mv FYC.dat NBU.fyc')
```


Now let's use classical campari to analyze it


```r
run_campari(data_file = "NBU.xtc", nsnaps = 10000, base_name = "xtc_based_analysis",
            FMCSC_SEQFILE = "nbu.in",
            FMCSC_CPROGINDMODE=1, #mst
            FMCSC_CCOLLECT=1, print_status = T, print_log_error = T,
            FMCSC_CMODE=4,
            FMCSC_CDISTANCE=7, #rmsd without alignment 7 - dihedral distances need a complete analysis (pdb_format dcd pdb etc...) 
            FMCSC_CPROGINDSTART=22, #starting snapshot 
            # FMCSC_CPROGINDRMAX=1000, #search att
            # FMCSC_BIRCHHEIGHT=2, #birch height
            FMCSC_CMAXRAD=6, #clustering
            FMCSC_CRADIUS=4,
            FMCSC_CCUTOFF=100,
            FMCSC_CPROGINDWIDTH=1000) #local cut is automatically adjusted to 1/10 if it is too big (as here)
            #FMCSC_CPROGMSTFOLD 4 # b)
run_campari(data_file = "NBU.xtc", nsnaps = 10000, base_name = "xtc_based_analysis",
            FMCSC_SEQFILE = "nbu.in",
            FMCSC_CPROGINDMODE=2, #mst
            FMCSC_CCOLLECT=1, print_status = T, print_log_error = T,
            FMCSC_CMODE=4,
            FMCSC_CDISTANCE=1, 
            FMCSC_CPROGINDSTART=29, #starting snapshot 
            FMCSC_CPROGINDRMAX=10000, #search att
            FMCSC_BIRCHHEIGHT=10, #birch height
            FMCSC_CMAXRAD=1, #clustering
            FMCSC_CRADIUS=0,
            FMCSC_CCUTOFF=100,
            FMCSC_CPROGRDEPTH=10,
            FMCSC_CPROGINDWIDTH=1000) #local cut is automatically adjusted to 1/10 if it is too big (as here)
            #FMCSC_CPROGMSTFOLD 4 # b)
sapphire_plot(sap_file = "PROGIDX_000000000029.dat", timeline= T, title = "ORIGINAL CAMPARI - MST")
```


