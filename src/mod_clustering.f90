!--------------------------------------------------------------------------!
! LICENSE INFO:                                                            !
!--------------------------------------------------------------------------!
!    This file is part of CAMPARI.                                         !
!                                                                          !
!    Version 2.0                                                           !
!                                                                          !
!    Copyright (C) 2014, The CAMPARI development team (current and former  !
!                        contributors)                                     !
!                        Andreas Vitalis, Adam Steffen, Rohit Pappu, Hoang !
!                        Tran, Albert Mao, Xiaoling Wang, Jose Pulido,     !
!                        Nicholas Lyle, Nicolas Bloechliger,               !
!                        Davide Garolini                                   !
!                                                                          !
!    Website: http://sourceforge.net/projects/campari/                     !
!                                                                          !
!    CAMPARI is free software: you can redistribute it and/or modify       !
!    it under the terms of the GNU General Public License as published by  !
!    the Free Software Foundation, either version 3 of the License, or     !
!    (at your option) any later version.                                   !
!                                                                          !
!--------------------------------------------------------------------------!
! AUTHORSHIP INFO:                                                         !
!--------------------------------------------------------------------------!
!                                                                          !
! MAIN:      Andreas Vitalis                                               !
! WRAPPER:   Davide Garolini                                               !
!                                                                          !
!--------------------------------------------------------------------------!
! trj_data            : data extracted for clustering (all read into memory -> large)
! n_snaps            : number of snapshots stored for clustering (-> trj_data)
! cmode              : choice of algorithm for clustering or similar tasks (1-5)
! dis_method          : the distance criterion underlying the clustering (1-8)
! cstorecalc         : frequency for collecting data for clustering
! cmaxsnaps          : allocation size for trj_data in numbers of snapshots dimension
! calcsz,clstsz      : dimensionality size variables for stored data (-> trj_data)
! cfilen             : input file for defining clustering subset
! cdofset/cl_imvec   : auxiliary variables for clustering data
! cradius            : general size threshold for clustering
! cprepmode          : options for signal (data) preprocessing
! cchangeweights     : for eligible dis_method, replace weights for distance evaluations
! cl_mwvec           : keep track of mean weight per dimension
! cwcombination      : for locally adaptive weights, how to combine (L_? norm)
! cwwindowsz         : for locally adaptive weights, how big a window to use
! cdynwbuf           : for LAWs dependent on transition counts, buffer parameter for taking inverse
! cwacftau           : for ACF-based weights, fixed lag time
! csmoothie          : order for smoothing fxn (sets window size)
! refine_clustering  : general flag to trigger refinement in clustering
! birchtree,scluster : arrays to hold clustering result
! sumssz             : global size variable for (t_scluster)%sums(:,:)
! cleadermode        : if cmode = 1/2/5(4): processing direction flags
! clinkage           : if cmode = 3: linkage criterion (1-3)
! chardcut           : if cmode = 3(4): truncation cut for snapshot NB-list
! nblfilen,read_nbl_from_nc,cnblst,cnc_ids : if cmode = 3(4): snapshot NB-list variables
! caddlkmd           : whether and how to augment network (add missing links)
! caddlinkwt         : what type of FP weight to use for added links
! cprogindex         : if cmode = 4: exact or approximate scheme (1-2)
! cprogindrmax       : if cmode = 4: number of nearest neighbor guesses for approximate scheme
! cprogindstart      : if cmode = 4: snapshot to start from
! cprogpwidth        : if cmode = 4: the (maximum) size of A/B partitions in localized cut
! cprogfold          : if cmode = 4: how often to collapse terminal vertices of the MST
! cprogrdepth        : if cmode = 4 and cprogindex = 2: auxiliary search depth
! cprogbatchsz       : if cmode = 4 and cprogindex = 2: batch size for random stretches
! csivmax,csivmin    : if cmode = 4: used to identify starting points automatically
! c_nhier            : if cmode = 5(4): tree height
! cmaxrad            : if cmode = 5(4): root level size threshold
! c_multires         : if cmode = 5(4): how many levels to recluster in multi-pass and print
! ccfepmode          : if any clustering is performed, type of cFEP to produce
! align_for_clustering: if dis_method = 5/6 whether to do alignment
! cdofsetbnds        : if dis_method = 6, selectors for separating sets for alignment and distance compu.
! pcamode            : whether to do PCA and what output to produce (1-3)
! reduced_dim_clustering: whether to proceed with clustering in reduced dimensional space after PCA
! align              : structure used for trajectory alignment (ALIGNFILE)
! csnap2tree         : temporary array for cmode = 4 and cprogindex = 2
! csnap2clus         : temporary array for cmode = 4 and cprogindex = 2
! tmptree            : temporary array for cmode = 4 and cprogindex = 2
! ntbrks,ntbrks2,itbrklst : management of user-requested trajectory breaks (counts and counter)
! ntlnks             : management of user-requested link additions (count)
!
module mod_clustering

  ! distance matrix must be allocatable
  real(8), allocatable :: distance_mat(:,:)

  ! cluster object
  type t_scluster
    integer nmbrs, alsz !alsz is the allocation size of the snaps principally
    real(8) sqsum
    real(8), ALLOCATABLE :: sums(:)
    integer, ALLOCATABLE :: snaps(:) ! snapshot lists
    integer, ALLOCATABLE :: children(:) !for tree-based clustering
    ! for tree-based clustering
    integer nchildren
    integer childr_alsz
    integer parent
    !for quality
    real(8) diam
    real(8) radius
    real(8) quality
    integer center
    !for SST
    integer, ALLOCATABLE :: tmpsnaps(:)
    ! nbalsz - resizenb - expand graph nb lists - we assume unallocated member arrays to be dealt with
    ! elsewhere
    ! like scluster_resize must never be called with allocation size currently zero
  end type t_scluster

  type t_ctree
    type(t_scluster), ALLOCATABLE:: cls(:) ! nothing but an array of clusters
    integer ncls, n_clu_alsz_tree ! real and allocation sizes
  end type t_ctree


  type(t_scluster), ALLOCATABLE :: scluster(:)
  type(t_ctree), ALLOCATABLE :: birchtree(:)
  real(8) radius !radius for clusters belonging limit
  real(8) cmaxrad
  integer n_clu_alc_sz_gen ! number of clusters allocatable in total
  integer nclu ! number of clusters allocated
  integer c_nhier !height of the tree
  integer c_multires !FMCSC_BIRCHMULTI initial default = 0
  integer ordering
  logical clu_summary !helper var for precise_clu_descr
  integer precise_clu_descr !the clusters are shortened and presented properly
  ! This are the sizes for sums variable that keeps the centroid measures.
  ! In the rmsd (dis_method = 5) only needs sumssz = 1 and clstsz = calcsz
  ! (calcsz is the number of atoms * coordinates)
  contains
    !--------------------------------------------------------------------
    ! LEADER CLUSTERING
    !--------------------------------------------------------------------
    ! each snapshot is added to a cluster on the base of a defined threshold.
    !The original structure is based on:
    !leader_clustering, gen_MST_from_nbl,do_prog_ind
    subroutine leader_clustering(trj)
      use mod_variables_gen
      use mod_gutenberg
      implicit none

      real(8), intent(in) :: trj(n_snaps,n_xyz)
      real(8) tmp_d ! temporary variable for radiuses and distances
      integer i, j, ii
      real(8) vecti(n_xyz)

      n_clu_alc_sz_gen = 2 ! a priori initial cluster numbers
      allocate(scluster(n_clu_alc_sz_gen))
      scluster(:)%nmbrs = 0 ! number of elements in each cluster
      scluster(:)%alsz = 0 ! allocation size of each cluster


      call sl()
      call spr('-----------------------------------')
      call spr('Now using truncated leader algorithm for pre-clustering...')
      call sl()

      nclu = 0 !cluster index
      do i=1,n_snaps
        ii = -1
        do j=nclu,max(nclu-500,1),-1 ! when nclu = 0 it skips this do  [j=0,1,-1]
          ! if(superver) write(ilog,*) "now checking dist(cluster,snap) : (",j,",",i,")"

          ! j is going down of -1, not nclu. nclu is growing if (ii.eq.-1)(new cluster is
          ! added). The number of clusters that can be considered in order to put i
          ! (snapshot) in one of those is 500. Once exceeded this upper bound
          ! it starts to shift on that limit forwards.
          vecti = trj(i,1:n_xyz)
          call snap_to_cluster_d(tmp_d, scluster(j), vecti)
          if (tmp_d.lt.radius) then
            call cluster_addsnap(scluster(j), vecti, i)
            ! add snapshot i in the cluster j
            ii = j
            exit
          end if
        end do
        if (ii.eq.-1) then
          ! a new cluster is added to the cluster list because tmp_d .gt. radius
          nclu = nclu + 1
          ! enlarge the size of the clusters array by the double
          if (nclu.gt.n_clu_alc_sz_gen) then
            call cluster_lst_resize(scluster)
          end if

          call cluster_addsnap(scluster(nclu),vecti,i) ! nclu new cluster
          ! if(superver) write(ilog,*) 'Cluster number ',nclu, ' created for snap ', i
        end if
      end do

      call sipr('... done with total number of clusters equal: ', nclu)
    end subroutine

! helper function to resize the cluster
    subroutine cluster_lst_resize(clu)
      use mod_variables_gen
      implicit none
      type(t_scluster), ALLOCATABLE :: tmpcls(:)
      type(t_scluster), ALLOCATABLE :: clu(:)
      integer oldsz
      allocate(tmpcls(n_clu_alc_sz_gen))
      oldsz = n_clu_alc_sz_gen
      tmpcls = clu
      deallocate(clu)
      n_clu_alc_sz_gen = n_clu_alc_sz_gen * 2
      allocate(clu(n_clu_alc_sz_gen))
      clu(1:oldsz) = tmpcls(1:oldsz)
      clu(oldsz+1:n_clu_alc_sz_gen)%nmbrs = 0
      clu(oldsz+1:n_clu_alc_sz_gen)%alsz = 0
      deallocate(tmpcls)
    end subroutine cluster_lst_resize


end module
